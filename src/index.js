let canvas = document.getElementById("myCanvas");
let divGameOver = document.querySelector(".gameover");
let youwin = document.querySelector(".youwin");
let ctx = canvas.getContext("2d");
let ballRadius = 10;
let x = canvas.width / 2;
let y = canvas.height - 30;
let dx = 2;
let dy = -2;

let paddle = {
    paddleHeight: 10,
    paddleWidth: 75,

}
let paddleX = (canvas.width - paddle.paddleWidth) / 2;
let rightPressed = false;
let leftPressed = false;
let brick = {
    brickRowCount: 5,
    brickColumnCount: 3,
    brickWidth: 75,
    brickHeight: 20,
    brickPadding: 10,
    brickOffsetTop: 30,
    brickOffsetLeft: 30
}

let ui = {
    score: 0,
    lives: 3
}



let bricks = [];
for (let c = 0; c < brick.brickColumnCount; c++) {
    bricks[c] = [];
    for (let r = 0; r < brick.brickRowCount; r++) {
        bricks[c][r] = { x: 0, y: 0, status: 1 };
    }
}
document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);
document.addEventListener("mousemove", mouseMoveHandler, false);

function keyDownHandler(e) {
    if (e.keyCode == 39) {
        rightPressed = true;
    } else if (e.keyCode == 37) {
        leftPressed = true;
    }
}

function keyUpHandler(e) {
    if (e.keyCode == 39) {
        rightPressed = false;
    } else if (e.keyCode == 37) {
        leftPressed = false;
    }
}

function mouseMoveHandler(e) {
    let relativeX = e.clientX - canvas.offsetLeft;
    if (relativeX > 0 && relativeX < canvas.width) {
        paddleX = relativeX - paddle.paddleWidth / 2;
    }
}

function collisionDetection() {
    for (let c = 0; c < brick.brickColumnCount; c++) {
        for (let r = 0; r < brick.brickRowCount; r++) {
            let b = bricks[c][r];
            if (b.status == 1) {
                if (x > b.x && x < b.x + brick.brickWidth && y > b.y && y < b.y + brick.brickHeight) {
                    dy = -dy;
                    b.status = 0;
                    ui.score++;
                    if (ui.score == brick.brickRowCount * brick.brickColumnCount) {;
                        youwin.classList.add('visible');
                        // document.location.reload();
                    }
                }
            }
        }
    }
}

function drawBall() {
    ctx.beginPath();
    ctx.arc(x, y, ballRadius, 0, Math.PI * 2);
    ctx.fillStyle = "#FF0000";
    ctx.fill();
    ctx.closePath();
}

function drawPaddle() {
    ctx.beginPath();
    ctx.rect(paddleX, canvas.height - paddle.paddleHeight, paddle.paddleWidth, paddle.paddleHeight);
    ctx.fillStyle = "#FF0000";
    ctx.fill();
    ctx.closePath();
}

function drawBricks() {
    for (let c = 0; c < brick.brickColumnCount; c++) {
        for (let r = 0; r < brick.brickRowCount; r++) {
            if (bricks[c][r].status == 1) {
                let brickX = (r * (brick.brickWidth + brick.brickPadding)) + brick.brickOffsetLeft;
                let brickY = (c * (brick.brickHeight + brick.brickPadding)) + brick.brickOffsetTop;
                bricks[c][r].x = brickX;
                bricks[c][r].y = brickY;
                ctx.beginPath();
                ctx.rect(brickX, brickY, brick.brickWidth, brick.brickHeight);
                ctx.fillStyle = "#FF0000";
                ctx.fill();
                ctx.closePath();
            }
        }
    }
}

function drawScore() {
    ctx.font = "16px Arial";
    ctx.fillStyle = "#FF0000";
    ctx.fillText("Score: " + ui.score, 8, 20);
}

function drawLives() {
    ctx.font = "16px Arial";
    ctx.fillStyle = "#FF0000";
    ctx.fillText("Lives: " + ui.lives, canvas.width - 65, 20);
}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawBricks();
    drawBall();
    drawPaddle();
    drawScore();
    drawLives();
    collisionDetection();
    if (x + dx > canvas.width - ballRadius || x + dx < ballRadius) {
        dx = -dx;
    }
    if (y + dy < ballRadius) {
        dy = -dy;
    } else if (y + dy > canvas.height - ballRadius) {
        if (x > paddleX && x < paddleX + paddle.paddleWidth) {
            dy = -dy;
        } else {
            ui.lives--;
            if (!ui.lives) {
                divGameOver.classList.add('visible');
            } else {
                x = canvas.width / 2;
                y = canvas.height - 30;
                dx = 3;
                dy = -3;
                paddleX = (canvas.width - paddle.paddleWidth) / 2;
            }
        }
    }
    if (rightPressed && paddleX < canvas.width - paddle.paddleWidth) {
        paddleX += 7;
    } else if (leftPressed && paddleX > 0) {
        paddleX -= 7;
    }
    x += dx;
    y += dy;
    requestAnimationFrame(draw);
}
draw();