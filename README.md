                             **Projet jeu JS "CASSE BRIQUE"**

![](./imagegame.png)

* L'objectif de ce projet était de faire un jeu en Javascript sans framework.
  Il va permettre d'utiliser le JS, l'algorithmie de base, la manipulation du DOM et la POO dans un contexte ludique mais professionnel.

* Le but du jeu est très simple, un barre qui fait rebondir la balle pour détruire les briques.

  La barre est manipulable via souris ou pad d'un ordinateur portable ou avec la flèche gauche & droite du clavier.

  le joueur a seulement 3 vies. Si il vient à rater de rattraper la balle , il perd une vie, et si il vient à perdre toutes ses vies , il perd le jeu.

  Si le joueur parvient à detruire toute les briques , il gagne le jeu.



##                                           Maquette

![](./maquette-brick-breaker.png)



#                                                                        UML 

![](./GameUseCase.png)





#                                                  DEMO DU JEU

![](./demogame.gif)

#                          Loosing the game



![](./demoloose.gif)

#                     Winning the Game

![](./gamewin.gif)





